### Preload

Geben Sie im ersten Abschnitt "preload" eine Liste von JavaScript, Styles oder Schriften an, die via rel=preload vorausgeladen werden sollen.
Protokoll und Host im URL-Feld können entfallen, wenn die Resourcen über dieselbe Domain wie Ihr Shop ausgeliefert werden.
 
Mögliche Werte für unterschiedliche Typen sind:
 
 * "style" für CSS
 * "script" für JavaScript
 * "image" für Bilder
 * "font" für Schriftarten
 
Weitere Informationen finden Sie in der [Spezifikation beim W3C](https://w3c.github.io/preload/).
 

### Prefetch

Im zweiten Abschnitt "prefetch" können URLs, die mit einer niedrigeren Priorität vorgelanden werden sollen, angegegeben werden.

Eine Angabe des Typs ist hier nicht nötig.

Einige Hinweise hierzu finden sich im [Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Web/HTTP/Link_prefetching_FAQ).


### DNS-Prefetch

Domains, die via DNS-Prefetching aufgelöst werden sollen, können Sie im dritten Abschnitt "DNS-Prefetch" eingeben.
Geben Sie hier nur die Domain, nicht den Link zu einer Datei an. Es sollten protokoll-relative URLs verwendet werden, wie beispielsweise

```
//example.com 
oder
//subdomain.example.com
```

### Robots ignorieren

Falls Sie nicht wünschen, dass die entsprechenden Header auch an Crawler gesendet werden, stellen Sie die Option "Auf Nicht-Bots beschränken?" auf "Ja".


### Testen

Um zu prüfen, ob das Plugin wie gewünscht funktioniert, löschen Sie zunächst den Cookie "JTLSHOP".

Im Chrome funktioniert dies beispielsweise über den Inspector -> Tab "Application" -> Cookies -> <Domain> -> Clear all.

Aktivieren Sie im Netzwerk-Tab anschließend die Spalte "Initiator" über einen Rechtsklick in den Tabellenüberschriften.
 
Wenn Sie anschließend die Seite neu laden, sollte in dieser Spalte in den Zeilen der vorgeladenen Dateien anschließend "Other" statt "(index)" zu sehen sein.

![Initiator](initiator.jpg "Initiator")

Filtern Sie andernfalls nach dem Typ "Doc", klicken Sie auf den ersten Eintrag und schauen dort im Response Header nach einem Eintrag für "Link".

![Response Header](response_header.jpg "Response Header") 




### weiterführende Links

* [Smashing Magazine](https://www.smashingmagazine.com/2016/02/preload-what-is-it-good-for/)
* [Wikipedia](https://en.wikipedia.org/wiki/Link_prefetching)
* [medium.com](https://medium.com/reloading/preload-prefetch-and-priorities-in-chrome-776165961bbf)
